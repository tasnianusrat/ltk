package ltkTest;

import org.testng.annotations.Test;

import pageObjectRepo.DiscoverPage;
import pageObjectRepo.HomePage;
import pageObjectRepo.SignUpPage;
import utilities.BaseTest;

public class LTKSignUpTest extends BaseTest {
	
	@Test(description = "Verify that user is able to sign up for LTK")
	public void verifyLTKSignUp(){
		HomePage homePage = new HomePage(driver);
		homePage.verifyLTKHomePage();
		homePage.clickOnSignUpButton();
		SignUpPage signUpPage = new SignUpPage(driver);
		signUpPage.verifyLTKSignUpPage();
		signUpPage.enterEmail("tasn@Gmail.com");
		signUpPage.enterPassWord("password");
		signUpPage.clickOnSignUpToBeginShopping();
		DiscoverPage discoverPage = new DiscoverPage(driver);
		discoverPage.verifyDiscoverPage();
	}
}
