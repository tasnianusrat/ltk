package pageObjectRepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class DiscoverPage {
	WebDriver driver;

    public DiscoverPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
  
    public void verifyDiscoverPage() {
    	Assert.assertEquals("Discover | LTK", driver.getTitle(), "Discover page is not loaded");
    	System.out.println("Validated that user is on LTK Discover page");
    }
}
