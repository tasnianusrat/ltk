package pageObjectRepo;

import org.testng.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignUpPage {
	WebDriver driver;

    public SignUpPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@name='email']")
    WebElement email;
    
    @FindBy(xpath = "//input[@name='password']")
    WebElement password;
    
    @FindBy(xpath = "//button//span[text()='sign up to begin shopping']")
    WebElement signUpToShopping;
  
    public void verifyLTKSignUpPage() {
    	Assert.assertEquals("Sign Up for an LTK Account | LTK", driver.getTitle(), "Sign Up page is not loaded");
    	System.out.println("Validated that user is on LTK sign up page");
    }
    
    public void enterEmail(String emailText) {
    	email.sendKeys(emailText);
    	System.out.println("Validated that user is able to enter email");
    }
    
    public void enterPassWord(String pass) {
    	password.sendKeys(pass);
    	System.out.println("Validated that user is able to enter password");
    }
    
    public void clickOnSignUpToBeginShopping(){
    	signUpToShopping.click();
    	System.out.println("Validated that user is able to click on sign up to begin shopping");

    }
}