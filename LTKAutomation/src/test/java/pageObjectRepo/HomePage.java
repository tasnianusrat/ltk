package pageObjectRepo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePage {
	WebDriver driver;

    public HomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//a[@href='/signup']")
    WebElement signUp;
  
    public void verifyLTKHomePage() {
    	Assert.assertEquals("LTK | Fashion, Home, Beauty, Fitness and More", driver.getTitle(), "Home page is not loaded");
    	System.out.println("Validated that user is on LTK Home page");
    }
    
    public void clickOnSignUpButton() {
    	signUp.click();
    	System.out.println("Validated that user is able to click on sign up button");
    }
}
