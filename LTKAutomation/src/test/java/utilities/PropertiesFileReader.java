package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFileReader {
	private static PropertiesFileReader INSTANCE = null;
	
	static Properties properties = new Properties();
	
	private PropertiesFileReader() {
		
	}
	
	public static PropertiesFileReader getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new PropertiesFileReader();
		}
		return INSTANCE;
	}
	
	public static Properties readProperties() {
		Properties prop = new Properties();
		try (InputStream inStream = new FileInputStream(".\\resource\\env.properties")) {
			
			prop.load(inStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;	
	}
	

}
