package utilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BaseTest {

	public static Properties envProperty = null;
	String osName = (System.getProperty("os.name")).substring(0, 3);
	protected WebDriver driver;
	
	static {
		PropertiesFileReader.getInstance();
		envProperty = PropertiesFileReader.readProperties();
	}

	public WebDriver getDriver() {
		return driver;
	}

	@Parameters("browser")
	@BeforeMethod(alwaysRun = true)
	protected void initializeTest(String browser) {
		switch (browser) {
		case "firefox":
				if (osName.equals("Win"))
				System.setProperty("webdriver.gecko.driver", "driver/geckodriver.exe");
			else
				System.out.println("Driver Property is not set for this OS ");
				driver = new FirefoxDriver();
			break;

		case "chrome":
			if (osName.equals("Win"))
				System.setProperty("webdriver.chrome.driver", "driver/chromedriver.exe");
			else
				System.out.println("Driver Property is not set for this OS ");
				driver = new ChromeDriver();
			break;

		default:
			WebDriverManager.chromedriver().setup();
			ChromeOptions option = new ChromeOptions();
			option.addArguments("--headless", "--disable-gpu", "--ignore-certificate-errors", "--disable-extensions",
					"--no-sandbox", "--disable-dev-shm-usage");
			driver = new ChromeDriver(option);
			break;
		}

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(envProperty.getProperty("testUrl"));		
	}

	@AfterMethod(alwaysRun = true)
    protected void tearDownTest() {
		driver.quit();		
	}
}